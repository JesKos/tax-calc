import Tax from './tax.mjs';

const CONF = {
    INITIAL_PRICE: 1,
    TAX_RATE: 24,
}

const DOM = {
    /** @type {HTMLInputElement} */
    LIST_PRICE: document.getElementById('list-price'),
    /** @type {HTMLInputElement} */
    TAX_RATE: document.getElementById('tax-rate'),
    /** @type {HTMLInputElement} */
    TOTAL_PRICE: document.getElementById('total-price')
}

const state = {
    /** @type {Array<number, number>} currently active fields */
    calc_based_on: ['list-price', 'tax-rate']
}

const changeValue = (field_name) => {
    if (!state.calc_based_on.includes(field_name)) {
        // active changed
        state.calc_based_on.unshift(field_name);
        state.calc_based_on.pop();
    } // else { // no change }
    if (state.calc_based_on.includes('list-price') && state.calc_based_on.includes('tax-rate')) {
        const list_price = parseFloat(DOM.LIST_PRICE.value);
        const tax_rate = parseFloat(DOM.TAX_RATE.value);
        DOM.TOTAL_PRICE.value = Tax.applyTax(list_price, tax_rate).toString();
    } else if (state.calc_based_on.includes('tax-rate') && state.calc_based_on.includes('total-price')) {
        const tax_rate = parseFloat(DOM.TAX_RATE.value);
        const total_price = parseFloat(DOM.TOTAL_PRICE.value);
        DOM.LIST_PRICE.value = Tax.deductTax(total_price, tax_rate).toString();
    } else { // includes 'total-price' && 'list-price'
        const total_price = parseFloat(DOM.TOTAL_PRICE.value);
        const list_price = parseFloat(DOM.LIST_PRICE.value);
        DOM.TAX_RATE.value = Tax.figureTaxRate(list_price, total_price);
    }
}

const setup = () => {
    DOM.LIST_PRICE.value = CONF.INITIAL_PRICE;
    DOM.TAX_RATE.value = CONF.TAX_RATE;
    DOM.LIST_PRICE.onchange = () => changeValue('list-price');
    DOM.TAX_RATE.onchange = () => changeValue('tax-rate');
    DOM.TAX_RATE.onchange();
    DOM.TOTAL_PRICE.onchange = () => changeValue('total-price');
}

setup();
